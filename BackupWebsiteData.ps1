# Are you running in 32-bit mode?
#   (\SysWOW64\ = 32-bit mode)

if ($PSHOME -like "*SysWOW64*")
{
  Write-Warning "Restarting this script under 64-bit Windows PowerShell."

  # Restart this script under 64-bit Windows PowerShell.
  #   (\SysNative\ redirects to \System32\ for 64-bit mode)

  & (Join-Path ($PSHOME -replace "SysWOW64", "SysNative") powershell.exe) -File `
    (Join-Path $PSScriptRoot $MyInvocation.MyCommand) @args

  # Exit 32-bit script.

  Exit $LastExitCode
}

# Was restart successful?
Write-Warning "Hello from $PSHOME"
Write-Warning "  (\SysWOW64\ = 32-bit mode, \System32\ = 64-bit mode)"
Write-Warning "Original arguments (if any): $args"


#Variables, only Change here
 $Destination="C:\BackUp" #Copy the Files to this Location
 $BackupDirs="C:\Sites\DemoAspWebFormApp" #What Folders you want to backup
 $Log="$env:username.txt" #Log Name
 $LoggingLevel="1" #LoggingLevel only for Output in Powershell Window, 1=smart, 3=Heavy


$Backupdir=$Destination +"\Backup-"+ (Get-Date -format MM-dd-yyyy)
$Items=0
$Count=0
$ErrorCount=0
$StartDate=Get-Date #-format dd.MM.yyyy-HH:mm:ss

#FUNCTION
#Logging
Function Logging ($State, $Message) {
$Datum=Get-Date -format MM/dd/yyyy

if (!(Test-Path -Path $Log)) {
    New-Item -Path $Log -ItemType File | Out-Null
}
$Text="$Datum - $State"+":"+" $Message"

if ($LoggingLevel -eq "1" -and $Message -notmatch "was copied") {Write-Host $Text}
elseif ($LoggingLevel -eq "3" -and $Message -match "was copied") {Write-Host $Text}

add-Content -Path $Log -Value $Text
}
Logging "INFO" "----------------------"
Logging "INFO" "Start the Script"


#Check if Backupdirs and Destination is available
function Check-Dir {

if (!(Test-Path $BackupDirs)) {
    return $false
    Logging "Error" "$BackupDirs does not exist"
}
if (!(Test-Path $Destination)) {
    return $false
    Logging "Error" "$Destination does not exist"
}
}

#Create Backupdir
Function Create-Backupdir {
Logging "INFO" "Create Backupdir $Backupdir"

Set-Location D:\IT\Backup_Log

}


#Save all the Files
Function Make-Backup {
Logging "INFO" "Started the Backup"
       $Files=@()
       $SumMB=0
       $SumItems=0
       $SumCount=0
       $colItems=0

foreach ($Backup in $BackupDirs) {
    $colItems = (Get-ChildItem $Backup -recurse | Where-Object {$_.mode -notmatch "h"} | Measure-Object -property length -sum) 
    $Items=0
    $FilesCount += Get-ChildItem $Backup -Recurse | Where-Object {$_.mode -notmatch "h"}
    $SumMB+=$colItems.Sum.ToString()
    $SumItems+=$colItems.Count
    }
$TotalMB="{0:N2}" -f ($SumMB / 1MB) + " MB of Files"
Logging "INFO" "There are $SumItems Files with  $TotalMB to copy"

foreach ($Backup in $BackupDirs) {
    $Index=$Backup.LastIndexOf("\")
    $SplitBackup=$Backup.substring(0,$Index)
    $Files = Get-ChildItem $Backup -Recurse | Where-Object {$_.mode -notmatch "h"} 
    foreach ($File in $Files) {
             $restpath = $file.fullname.replace($SplitBackup,"")
        try {
            Copy-Item  $file.fullname $($Backupdir+$restpath) -Force -ErrorAction SilentlyContinue |Out-Null

        }
        catch {
            $ErrorCount++
            Logging "ERROR" "$file returned an error an was not copied"
        }
        $Items += (Get-item $file.fullname).Length
        $status = "Copy file {0} of {1} and copied {3} MB of {4} MB: {2}" -f $count,$SumItems,$file.Name,("{0:N2}" -f ($Items / 1MB)).ToString(),("{0:N2}" -f ($SumMB / 1MB)).ToString()
        $Index=[array]::IndexOf($BackupDirs,$Backup)+1
        $Text="Copy data Location {0} of {1}" -f $Index ,$BackupDirs.Count
        Write-Progress -Activity $Text $status -PercentComplete ($Items / $SumMB*100)  
        if ($File.Attributes -ne "Directory") {$count++}
    }
}
$SumCount+=$Count
$SumTotalMB="{0:N2}" -f ($Items / 1MB) + " MB of Files"
Logging "INFO" "Copied $SumCount files with $SumTotalMB"
Logging "INFO" "$ErrorCount Files could not be copied"
}



#Check if all Dir are existing and do the Backup
$CheckDir=Check-Dir

if ($CheckDir -eq $false) {
Logging "ERROR" "One of the Directory are not available, Script has stopped"
} else {
Make-Backup

$Enddate=Get-Date #-format dd.MM.yyyy-HH:mm:ss
$span = $EndDate - $StartDate
$Minutes=$span.Minutes
$Seconds=$Span.Seconds

Logging "INFO" "Backup duration $Minutes Minutes and $Seconds Seconds"
Logging "INFO" "All Done!"
Logging "INFO" "----------------------"

Logging "Info" "-----------------------------"
Logging "Info" "Start Delete folder Contents and Folder"
Get-ChildItem -Path  $BackupDirs -Recurse | Remove-Item -force -recurse
Remove-Item  $BackupDirs -Force 
Logging "Info" "Complete Deletion"


}

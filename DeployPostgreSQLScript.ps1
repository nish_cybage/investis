Param
(
  [string]$DBPort,
  [string]$DBUser,
  [string]$DBName,
  [string]$DBHostName,
  [string]$ScriptFilePath
)
# Are you running in 32-bit mode?
#   (\SysWOW64\ = 32-bit mode)

if ($PSHOME -like "*SysWOW64*")
{
  Write-Warning "Restarting this script under 64-bit Windows PowerShell."

  # Restart this script under 64-bit Windows PowerShell.
  #   (\SysNative\ redirects to \System32\ for 64-bit mode)

  & (Join-Path ($PSHOME -replace "SysWOW64", "SysNative") powershell.exe) -File `
    (Join-Path $PSScriptRoot $MyInvocation.MyCommand) @args

  # Exit 32-bit script.

  Exit $LastExitCode
}

# Was restart successful?
Write-Warning "Hello from $PSHOME"
Write-Warning "  (\SysWOW64\ = 32-bit mode, \System32\ = 64-bit mode)"
Write-Warning "Original arguments (if any): $args"

Write-Host "Deploy script on Postgresql database - Start"
#$env:PGPASSFILE= "C:\Users\Administrator\AppData\Roaming\postgresql\pgpass.conf"
$PGPASSWORD='cybage123'
try
{
  Write-Warning "Start Execute DB"
  Start-Process 'C:\Program Files\PostgreSQL\9.5\bin\psql.exe' -ArgumentList "-h $DBHostName -p $DBPort -U $DBUser -d $DBName -f $ScriptFilePath" -RedirectStandardError c:\tracerr.txt
  Write-Warning "Stop Execute DB"
}
catch
{
   Write-Output "Ran into an issue: $($PSItem.ToString())"
}

Write-Host "Deploy script on Postgresql database - END"
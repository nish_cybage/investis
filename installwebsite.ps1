# Are you running in 32-bit mode?
#   (\SysWOW64\ = 32-bit mode)

if ($PSHOME -like "*SysWOW64*")
{
  Write-Warning "Restarting this script under 64-bit Windows PowerShell."

  # Restart this script under 64-bit Windows PowerShell.
  #   (\SysNative\ redirects to \System32\ for 64-bit mode)

  & (Join-Path ($PSHOME -replace "SysWOW64", "SysNative") powershell.exe) -File `
    (Join-Path $PSScriptRoot $MyInvocation.MyCommand) @args

  # Exit 32-bit script.

  Exit $LastExitCode
}

# Was restart successful?
Write-Warning "Hello from $PSHOME"
Write-Warning "  (\SysWOW64\ = 32-bit mode, \System32\ = 64-bit mode)"
Write-Warning "Original arguments (if any): $args"

Import-Module WebAdministration

$iisAppPoolName = "DemoAspWebFormApp"
$iisAppPoolDotNetVersion = "v4.5"
$iisAppName = "DemoAspWebFormApp"
$directoryPath = "C:\Sites\DemoAspWebFormApp\Inves\Inves"

#check if the app pool exists
if (!(Test-Path "IIS:\AppPools\$iisAppPoolName" -pathType container))
{
    #create the app pool
    $appPool = New-WebAppPool $iisAppPoolName
    #$appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion

    Write-Host "Application Pool Created Successfully"
}


#check if the site exists
if (Test-Path "IIS:\Sites\$iisAppName" -pathType container)
{
    Write-Host "Website Already Exists"
    return
}


#create the site with multiple bindings

$ipV4 = ([System.Net.DNS]::GetHostAddresses($env:computername)|Where-Object {$_.AddressFamily -eq "InterNetwork"}   |  select-object IPAddressToString)[0].IPAddressToString

$website = New-Website -Name $iisAppName -PhysicalPath $directoryPath -ApplicationPool $iisAppPoolName -Port 80
$website = New-WebBinding -Name $iisAppName -IPAddress $ipV4 -Port 80 -HostHeader ""


Write-Host "Website Created Successfully" 

Write-Host "Website Created Successfully"
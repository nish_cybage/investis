# Are you running in 32-bit mode?
#   (\SysWOW64\ = 32-bit mode)

if ($PSHOME -like "*SysWOW64*")
{
  Write-Warning "Restarting this script under 64-bit Windows PowerShell."

  # Restart this script under 64-bit Windows PowerShell.
  #   (\SysNative\ redirects to \System32\ for 64-bit mode)

  & (Join-Path ($PSHOME -replace "SysWOW64", "SysNative") powershell.exe) -File `
    (Join-Path $PSScriptRoot $MyInvocation.MyCommand) @args

  # Exit 32-bit script.

  Exit $LastExitCode
}

# Was restart successful?
Write-Warning "Hello from $PSHOME"
Write-Warning "  (\SysWOW64\ = 32-bit mode, \System32\ = 64-bit mode)"
Write-Warning "Original arguments (if any): $args"

Import-Module WebAdministration

$iisAppPoolName = "DemoAspWebFormApp"
$iisAppName = "DemoAspWebFormApp"

try
{
    #check if the site exists
    if (Test-Path "IIS:\Sites\$iisAppName") {
        "Removing existing website $iisAppName"
        Remove-Website -Name $iisAppName
    }    

    #check if the app pool exists
    if (Test-Path "IIS:\AppPools\$iisAppPoolName") {
        "Removing existing AppPool $iisAppPoolName"
         Remove-WebAppPool -Name $iisAppPoolName
    }

}
catch
{
    "Some error occured"
}
@echo off

REM scheduled task point to .bat files
REM besides we need to make sure we have system variables in place

REM export a password for use with the system (no quotes)
SET PGHOST=%1
SET PGDATABASE=%2
SET PGUSER=%3
SET PGPASSWORD=%4

REM execute psql by file, even though echo is off, errors will still show

"C:\Program Files\PostgreSQL\9.5\bin\psql.exe" -X --variable=ON_ERROR_STOP= -1 -w -f "%5"

